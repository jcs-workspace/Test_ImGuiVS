[![C++ Standard](https://img.shields.io/badge/standard-17-black.svg?style=flat&logo=cplusplus)]()
[![Vulkan](https://img.shields.io/badge/vulkan-1.3.243.0-9C1C20.svg?style=flat&logo=vulkan)](https://www.vulkan.org/)

# Test_ImGuiVS

Test project to use [ImGui](https://github.com/ocornut/imgui) from scratch. (Visual Studio 2022)

The catch here is to compile ImGui with your project since it doesn't provide
static link library.
